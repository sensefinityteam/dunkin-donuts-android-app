package com.sensefinity.dunkindonuts;


/**
 * Created by joao on 20/11/2015.
 */
public class Transaction {

    private ProductItem product;
    private TrasactionState state;

    private enum TrasactionState{Completed,processing,pending}

    public void Transaction(ProductItem product, TrasactionState state) {

        this.product = product;
        this.state = state;
    }

    public ProductItem getProduct() {

        return product;
    }

    public void setProduct(ProductItem product) {

        this.product = product;
    }

    public TrasactionState getState() {

        return state;
    }

    public void setState(TrasactionState state) {

        this.state = state;
    }
}

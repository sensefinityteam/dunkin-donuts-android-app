package com.sensefinity.dunkindonuts;

/**
 * Created by RuiPires on 01/12/2015.
 */
public interface OnTaskCompleted {

    void onTaskCompleted(Boolean taskStatus);
}
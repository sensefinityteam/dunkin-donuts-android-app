package com.sensefinity.dunkindonuts;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.util.Log;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.Serializable;

/**
 * Created by RuiPires on 23/11/2015.
 */
public class ProductItem implements Serializable {

    String name;
    double price;
    byte[] bitmapDrawable;

    public void setBitmapDrawable(Bitmap bitmap) {

        try {
            this.writeObject(bitmap);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public BitmapDrawable getBitmapDrawable() {

        try {
            return this.readObject();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        return null;
    }

    public Bitmap getBitmap() {

        ByteArrayInputStream in = new ByteArrayInputStream(bitmapDrawable);
        ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
        int b;
        while((b = in.read()) != -1)
            byteStream.write(b);
        byte bitmapBytes[] = byteStream.toByteArray();
        Bitmap bitmap = BitmapFactory.decodeByteArray(bitmapBytes, 0, bitmapBytes.length);

        return bitmap;
    }


    public ProductItem(String name, double price){

        this.name = name;
        this.price = price;
        this.bitmapDrawable = null;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    // Converts the Bitmap into a byte array for serialization
    private void writeObject(Bitmap bitmap) throws IOException {

        ByteArrayOutputStream byteStream = new ByteArrayOutputStream();

        bitmap.compress(Bitmap.CompressFormat.PNG, 0, byteStream);
        bitmapDrawable = byteStream.toByteArray();
    }

    // Deserializes a byte array representing the Bitmap and decodes it
    private BitmapDrawable readObject() throws IOException, ClassNotFoundException {

        ByteArrayInputStream in = new ByteArrayInputStream(bitmapDrawable);
        ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
        int b;
        while((b = in.read()) != -1)
            byteStream.write(b);
        byte bitmapBytes[] = byteStream.toByteArray();
        BitmapDrawable bitmap = new BitmapDrawable(BitmapFactory.decodeByteArray(bitmapBytes, 0, bitmapBytes.length));
        return bitmap;
    }
}


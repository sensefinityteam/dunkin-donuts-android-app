package com.sensefinity.dunkindonuts.productsTabs;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.sensefinity.dunkindonuts.Activities.Tab1;
import com.sensefinity.dunkindonuts.Activities.Tab2;
import com.sensefinity.dunkindonuts.Activities.Tab3;

/*
*  Fragment Tabs adapter
* */
public class ViewPagerAdapter extends FragmentPagerAdapter {

    // Tabs fragments
    public final Tab1 tab1;
    private final Tab2 tab2;
    private final Tab3 tab3;

    CharSequence Titles[]; // Titles of the Tabs
    int NumbOfTabs; // Number of tabs


    // Constructor, assign the passed Values
    public ViewPagerAdapter(FragmentManager fm,CharSequence mTitles[], int mNumbOfTabsumb) {

        super(fm);

        this.Titles = mTitles;
        this.NumbOfTabs = mNumbOfTabsumb;

        //Create all fragments
        tab1 = new Tab1();
        tab2 = new Tab2();
        tab3 = new Tab3();
       }


    // Return the fragment for every position
    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0: // if the position is 0 we are returning the First tab
                return tab1;
            case 1:
                return tab2;
            case 2:
                return tab3;
            default:
                break;
        }
        return null;
    }


    // Titles for the Tabs in the Tab Strip
    @Override
    public CharSequence getPageTitle(int position) {
        return Titles[position];
    }

    // Number of tabs for the tabs Strip
    @Override
    public int getCount() {
        return NumbOfTabs;
    }

}

package com.sensefinity.dunkindonuts.productsTabs;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.util.Base64;
import android.util.Log;
import android.widget.BaseAdapter;

import com.sensefinity.dunkindonuts.Activities.NewOrderActivity;
import com.sensefinity.dunkindonuts.OnTaskCompleted;
import com.sensefinity.dunkindonuts.ProductItem;
import com.sensefinity.dunkindonuts.R;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class GetProductsFromMachinates extends AsyncTask<String, Void, String> {

    private static final String TAG = GetProductsFromMachinates.class.getName() ;

    private Context context;

    private Boolean taskStatus=false;
    private NewOrderActivity listener= null;

    public GetProductsFromMachinates(OnTaskCompleted listener){
        this.listener= (NewOrderActivity) listener;
    }

    public GetProductsFromMachinates(){
        this.listener= listener;
    }

    @Override
    protected String doInBackground(String... urls) {

        /*StringBuilder builder = new StringBuilder();
        HttpClient client = new DefaultHttpClient();
        HttpGet httpGet = new HttpGet("http://machinates.sensefinity.com/service/products");

        httpGet.addHeader("X-Machinates-Application-Key", "Machinates");
        httpGet.addHeader("Authorization", "Basic c2Vuc2VmaW5pdHlcQWRtaW46c2Vuc2UyMDE0Iw==");
        httpGet.addHeader("Accept", "application/json");

        try {

            HttpResponse response = client.execute(httpGet);
            StatusLine statusLine = response.getStatusLine();
            int statusCode = statusLine.getStatusCode();

            if(statusCode == 200) {

                HttpEntity entity = response.getEntity();
                InputStream content = entity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(content));

                String line;
                while((line = reader.readLine()) != null) {

                    builder.append(line);
                }

                try {

                    JSONObject json_object = new JSONObject(builder.toString());
                    JSONArray items = json_object.getJSONObject("value").getJSONArray("items");

                    for(int i = 0; i < items.length(); i++) {

                        //Log.d(SEARCH_SERVICE, items.getJSONObject(i).getString("photo"));
                        ProductItem new_product = new ProductItem(items.getJSONObject(i).getString("name"), Double.parseDouble(items.getJSONObject(i).getString("price")));

                        NewOrderActivity.beveragesList.add(new_product);
                        //items.getJSONObject(i).getString("category"),
                        //items.getJSONObject(i).getString("price"));

                        if(items.getJSONObject(i).getString("photo").contains("data:image/jpeg;base64,")) {

                            String image_base64_data = items.getJSONObject(i).getString("photo").substring(
                                    items.getJSONObject(i).getString("photo").indexOf(",") + 1);

                            InputStream stream = new ByteArrayInputStream(Base64.decode(image_base64_data.getBytes(), Base64.DEFAULT));

                            //new_product.setBitmapDrawable((Bitmap) (BitmapDrawable.createFromStream(stream,"name")));
                        }
                    }

                    taskStatus=true;
                    NewOrderActivity.GetProductsFromMachinatesDone = true;
                    Log.d(TAG, "GetProductsFromMachinates Successful Completed");

                } catch (JSONException e) {

                    Log.d(TAG, "GetProductsFromMachinates  JSONException");
                    e.printStackTrace();
                }
            }
        } catch(ClientProtocolException e){

            Log.d(TAG, "GetProductsFromMachinates  ClientProtocolException");
            e.printStackTrace();
        } catch (IOException e){

            Log.d(TAG, "GetProductsFromMachinates  IOException");
            e.printStackTrace();
        }*/

        listener.addYonestProducts();

        return null;
    }

    @Override
    protected void onPostExecute(String result) {

       listener.UpdateTabs();

        //Toast.makeText(context, "Trying to load Products list, please wait", Toast.LENGTH_LONG).show();
       Log.d(TAG,"onPostExecute taskStatus-"+ taskStatus);
        if(listener!=null) listener.onTaskCompleted(taskStatus);
        taskStatus=false;
    }
}
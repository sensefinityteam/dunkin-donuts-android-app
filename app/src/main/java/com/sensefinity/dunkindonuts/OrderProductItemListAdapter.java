package com.sensefinity.dunkindonuts;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.sensefinity.dunkindonuts.Activities.NewOrderActivity;

import java.lang.ref.WeakReference;
import java.util.List;


/* Product item list View Adapter
*   One for each list view, of each tab
* */
public class OrderProductItemListAdapter extends ArrayAdapter<ProductItem> {

    private static final String TAG = OrderProductItemListAdapter.class.getName();

    // The list
    private List<ProductItem> itemsList;
    private Context context;

    // Constructor,  passed list
    public OrderProductItemListAdapter(Context context, int layoutResourceId, List<ProductItem> items) {

        super(context, layoutResourceId, items);
        this.context = context;
        this.itemsList = items;
    }

    @Override
    public int getCount() {
        return itemsList.size();
    }

    @Override
    public ProductItem getItem(int arg0) {
        return itemsList.get(arg0);
    }

    @Override
    public long getItemId(int arg0) {
        return arg0;
    }

    // Inner class to save item view info
    public static class ProductItemHolder {
        ProductItem productItem;
        TextView pName;
        TextView pPrice;
        ImageView pImage;
        ImageButton addButton;
    }

    /*
    * Return specific view for each position
    */
    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {

        View row = convertView;

        final ProductItemHolder productHolder = new ProductItemHolder();
        productHolder.productItem = itemsList.get(position);

        LayoutInflater inflater = ((Activity) context).getLayoutInflater();
        // LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        row = inflater.inflate(R.layout.product_item, parent, false);

        productHolder.pName = (TextView) row.findViewById(R.id.mybag_pname);
        productHolder.pPrice = (TextView) row.findViewById(R.id.products_pprice);

        productHolder.addButton = (ImageButton) row.findViewById(R.id.products_addbutton);
        productHolder.addButton.setTag(productHolder.addButton);

        productHolder.pImage = (ImageView) row.findViewById(R.id.mybag_imageView);

        /*
        productHolder.delButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Log.d(TAG, "button onclick()");
            }
        });
        */
        row.setTag(productHolder);

        productHolder.addButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                NewOrderActivity.addProduct(productHolder.productItem);
                Log.d(TAG, "Add product to my bag");
                Toast.makeText(context, "Product added to bag.", Toast.LENGTH_SHORT).show();

                // ((ListView) parent).performItemClick(v, position, 0); // Let the event be handled in onItemClick()
            }
        });


        productHolder.pPrice.setText(productHolder.productItem.getPrice() + "€");
        productHolder.pName.setText(productHolder.productItem.getName());

        loadBitmap(position, productHolder.pImage);

        return row;
    }

    class BitmapWorkerTask extends AsyncTask<Integer, Void, BitmapDrawable> {
        private final WeakReference<ImageView> imageViewReference;
        private int data = 0;

        public BitmapWorkerTask(ImageView imageView) {
            // Use a WeakReference to ensure the ImageView can be garbage collected
            imageViewReference = new WeakReference<ImageView>(imageView);
        }

        // Decode image in background.
        @Override
        protected BitmapDrawable doInBackground(Integer... params) {
            data = params[0];
            return itemsList.get(data).getBitmapDrawable();
        }

        // Once complete, see if ImageView is still around and set bitmap.
        @Override
        protected void onPostExecute(BitmapDrawable bitmap) {

            if (isCancelled()) {
                bitmap = null;
            }

            if (imageViewReference != null && bitmap != null) {
                final ImageView imageView = imageViewReference.get();
                if (imageView != null) {
                    imageView.setImageDrawable(bitmap);
                }
            }
        }
    }

    public void loadBitmap(int resId, ImageView imageView) {

        final BitmapWorkerTask task = new BitmapWorkerTask(imageView);
        task.execute(resId);
    }
}
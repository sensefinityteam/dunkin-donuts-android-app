package com.sensefinity.dunkindonuts;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.support.design.widget.FloatingActionButton;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.sensefinity.dunkindonuts.Activities.MyBagActivity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;

import java.lang.ref.WeakReference;
import java.util.List;


/* Product item list View Adapter
*   One for each list view, of each tab
* */
public class myBagProductsListAdapter extends ArrayAdapter<ProductItem>{

    private static final String TAG = myBagProductsListAdapter.class.getName();

    // The list
    private List<ProductItem> itemsList;
    private MyBagActivity context;

    // Constructor,  passed list
    public myBagProductsListAdapter(MyBagActivity context, int layoutResourceId, List<ProductItem> items) {

        super(context, layoutResourceId, items);
        this.context = context;
        this.itemsList = items;
    }

    @Override
    public int getCount() {
        return itemsList.size();
    }

    @Override
    public ProductItem getItem(int arg0) {
        return itemsList.get(arg0);
    }

    @Override
    public long getItemId(int arg0) {
        return arg0;
    }

    // Inner class to save item view info
    public static class BagProductItemHolder {
        ProductItem productItem;
        TextView pName;
        TextView pPrice;
        ImageView pImage;
        FloatingActionButton delButton;
    }

    /*
    * Return specific view for each position
    */
    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {

        View row = convertView;

        BagProductItemHolder productHolder= new BagProductItemHolder();
        productHolder.productItem = itemsList.get(position);

        LayoutInflater inflater = ((Activity) context).getLayoutInflater();
        // LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        row = inflater.inflate(R.layout.mybag_product_item, parent, false);

        productHolder.pName = (TextView)row.findViewById(R.id.mybag_pname);
        productHolder.pPrice = (TextView) row.findViewById(R.id.myBag_pprice);

        productHolder.delButton = (FloatingActionButton)row.findViewById(R.id.mybag_delbutton);
        productHolder.delButton.setTag(productHolder.delButton);

        productHolder.pImage = (ImageView) row.findViewById(R.id.mybag_imageView);

        row.setTag(productHolder);

        productHolder.delButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Log.d(TAG, "Del button onclick()");
                MyBagActivity.deleteProduct(position);

                Log.d(TAG, "Deleted product");
                Toast.makeText(context, "Product deleted.", Toast.LENGTH_SHORT).show();

                //Toast.makeText(context, "Button DEl clicked", Toast.LENGTH_SHORT).show();
                // ((ListView) parent).performItemClick(v, position, 0); // Let the event be handled in onItemClick()
            }
        });


        productHolder.pPrice.setText(productHolder.productItem.getPrice() + "€");
        productHolder.pName.setText(productHolder.productItem.getName());

        loadBitmap(position, productHolder.pImage);

        return row;
    }

    class BitmapWorkerTask extends AsyncTask<Integer, Void, BitmapDrawable> {
        private final WeakReference<ImageView> imageViewReference;
        private int data = 0;

        public BitmapWorkerTask(ImageView imageView) {
            // Use a WeakReference to ensure the ImageView can be garbage collected
            imageViewReference = new WeakReference<ImageView>(imageView);
        }

        // Decode image in background.
        @Override
        protected BitmapDrawable doInBackground(Integer... params) {
            data = params[0];
            return itemsList.get(data).getBitmapDrawable();
        }

        // Once complete, see if ImageView is still around and set bitmap.
        @Override
        protected void onPostExecute(BitmapDrawable bitmap) {

            if (isCancelled()) {
                bitmap = null;
            }

            if (imageViewReference != null && bitmap != null) {
                final ImageView imageView = imageViewReference.get();
                if (imageView != null) {
                    imageView.setImageDrawable(bitmap);
                }
            }
        }
    }

    public void loadBitmap(int resId, ImageView imageView) {

        final BitmapWorkerTask task = new BitmapWorkerTask(imageView);
        task.execute(resId);
    }
}

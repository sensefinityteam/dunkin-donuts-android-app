package com.sensefinity.dunkindonuts;


import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.sensefinity.dunkindonuts.Activities.NewOrderActivity;

import java.lang.ref.WeakReference;
import java.util.List;

public class OrderItemListAdapter extends ArrayAdapter<ProductItem> {

    private static final String TAG = OrderItemListAdapter.class.getName();

    // The list
    private List<ProductItem> itemsList;
    private Context context;

    // Constructor,  passed list
    public OrderItemListAdapter(Context context, int layoutResourceId, List<ProductItem> items) {

        super(context, layoutResourceId, items);
        this.context = context;
        this.itemsList = items;
    }

    @Override
    public int getCount() {
        return itemsList.size();
    }

    @Override
    public ProductItem getItem(int arg0) {
        return itemsList.get(arg0);
    }

    @Override
    public long getItemId(int arg0) {
        return arg0;
    }

    // Inner class to save item view info
    public static class OrderProductItemHolder {
        ProductItem productItem;
        TextView pName;
        TextView pPrice;
        ImageView pImage;
    }

    /*
    * Return specific view for each position
    */
    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {

        View row;

        OrderProductItemHolder productHolder= new OrderProductItemHolder();

        productHolder.productItem = itemsList.get(position);
        LayoutInflater inflater = ((Activity) context).getLayoutInflater();

        row = inflater.inflate(R.layout.order_product_item, parent, false);

        productHolder.pName = (TextView)row.findViewById(R.id.order_pname);
        productHolder.pPrice = (TextView) row.findViewById(R.id.order_pprice);
        productHolder.pImage = (ImageView) row.findViewById(R.id.order_imageView);

        row.setTag(productHolder);

        productHolder.pPrice.setText(productHolder.productItem.getPrice() + "€");
        productHolder.pName.setText(productHolder.productItem.getName());

        loadBitmap(position, productHolder.pImage);

        return row;
    }

    class BitmapWorkerTask extends AsyncTask<Integer, Void, BitmapDrawable> {
        private final WeakReference<ImageView> imageViewReference;
        private int data = 0;

        public BitmapWorkerTask(ImageView imageView) {
            // Use a WeakReference to ensure the ImageView can be garbage collected
            imageViewReference = new WeakReference<ImageView>(imageView);
        }

        // Decode image in background.
        @Override
        protected BitmapDrawable doInBackground(Integer... params) {
            data = params[0];
            return itemsList.get(data).getBitmapDrawable();
        }

        // Once complete, see if ImageView is still around and set bitmap.
        @Override
        protected void onPostExecute(BitmapDrawable bitmap) {

            if (isCancelled()) {
                bitmap = null;
            }

            if (imageViewReference != null && bitmap != null) {
                final ImageView imageView = imageViewReference.get();
                if (imageView != null) {
                    imageView.setImageDrawable(bitmap);
                }
            }
        }
    }

    public void loadBitmap(int resId, ImageView imageView) {

        final BitmapWorkerTask task = new BitmapWorkerTask(imageView);
        task.execute(resId);
    }
}

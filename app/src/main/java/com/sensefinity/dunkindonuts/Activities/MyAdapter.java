package com.sensefinity.dunkindonuts.Activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.sensefinity.dunkindonuts.R;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {

    private static final String TAG = MyAdapter.class.getName();

    // To know which view is being worked on, header or Item type
    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;

    private String mNavTitles[]; // Store the passed titles values from BaseActivity
    private int mIcons[];        // Store the passed icons resource value from BaseActivity


    /* User profile */
    private String name;        //String resource for header View Name
    private int profile;        //int resource for  profile picture

    private static Context ctx;


    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {

        Log.d(TAG,"MyAdapter");
        super.onAttachedToRecyclerView(recyclerView);
    }

    /*
        Creating a ViewHolder class which extends the RecyclerView View Holder
        ViewHolder are used to store the inflated views in order to recycle them
         */
    public static class ViewHolder extends RecyclerView.ViewHolder{

        int Holderid;
        TextView textView;
        ImageView imageView;
        ImageView profile;
        TextView Name;

        // ViewHolder Constructor with View and viewType as a parameter
        public ViewHolder(View itemView,int ViewType) {
            super(itemView);

            // Here we set the appropriate view in accordance with the the view type.

            if(ViewType == TYPE_ITEM) {
                textView = (TextView) itemView.findViewById(R.id.rowText);
                imageView = (ImageView) itemView.findViewById(R.id.rowIcon);
                Holderid = 1;       // Setting holder id as 1 -> type item
            }
            else{
                Name = (TextView) itemView.findViewById(R.id.name);
                profile = (ImageView) itemView.findViewById(R.id.circleView);
                Holderid = 0;      // Setting holder id = 0 -> type header
            }
        }

    }


    /* MyAdapter Constructor */
    MyAdapter(String Titles[],int Icons[],String Name, int Profile, Context tx){
        mNavTitles = Titles;
        mIcons = Icons;
        name = Name;
        profile = Profile;
        ctx=tx;
    }




    /* onCreateViewHolder is called when the ViewHolder is created
    * in this method we inflate the layouts and pass it to the view holder
    */
    @Override
    public MyAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        Log.d(TAG,"onCreateViewHolder");

        if (viewType == TYPE_ITEM) {
            //Inflating the layout
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_row,parent,false);
            ViewHolder vhItem = new ViewHolder(v,viewType); //Creating ViewHolder, passing view type
            return vhItem; // Return ViewHolder

        } else if (viewType == TYPE_HEADER) {

            //Inflating the layout
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.header,parent,false);
            ViewHolder vhHeader = new ViewHolder(v,viewType); //Creating ViewHolder, passing view type
            return vhHeader; //Return ViewHolder

        }
        return null;
    }

    /* onBindViewHolder method is called when the item in a row is needed to be displayed,
    * the position tells us item at which position is being constructed to be displayed and
    * the holder id of the holder object tell us which view type is being created 1 for item row
    * */
    @Override
    public void onBindViewHolder(MyAdapter.ViewHolder holder,final int position) {

        Log.d(TAG,"onBindViewHolder");
        holder.itemView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                //Log.d(TAG, "Navigator drawer -> Click on "+ position);
                //Toast.makeText(ctx, "Navigator drawer -> Click on "+ position, Toast.LENGTH_SHORT).show();
                Intent intent;
                switch (position){
                    case 0:
                        BaseActivity.closeNavigatorDrawer();
                        break;
                    case 1:
                        BaseActivity.closeNavigatorDrawer();
                        intent = new Intent(ctx, MainActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        ctx.startActivity(intent);
                        break;
                    case 2:
                        BaseActivity.closeNavigatorDrawer();
                        intent = new Intent(ctx, NewOrderActivity.class);
                        ctx.startActivity(intent);
                        break;
                    case 3:
                        BaseActivity.closeNavigatorDrawer();
                        intent = new Intent(ctx, MyBagActivity.class);
                        intent.setClass(ctx,MyBagActivity.class);
                        // Create a Bundle and Put Bundle in to it
                        Bundle bundleObject = new Bundle();

                        bundleObject.putSerializable("list", NewOrderActivity.mybag);
                        intent.putExtra("key",bundleObject);
                        ctx.startActivity(intent);
                        break;
                    case 4:
                        BaseActivity.closeNavigatorDrawer();
                        //intent = new Intent(ctx, ProfileActivity.class);
                        //ctx.startActivity(intent);
                        break;
                    case 5:
                        BaseActivity.closeNavigatorDrawer();
                        break;
                    default:
                        BaseActivity.closeNavigatorDrawer();
                        break;
                }
            }
        });

        //Item type
        if(holder.Holderid ==1) {

            holder.textView.setText(mNavTitles[position - 1]);      // Set the Text VIEW
            holder.imageView.setImageResource(mIcons[position -1]); // Set the ICON image
        }
        else{
        //Header Type
            holder.profile.setImageResource(profile);               // Set profile image
            holder.Name.setText(name);                              // Set user name
        }
    }

    // Returns the number of items present in the list
    @Override
    public int getItemCount() {
        return mNavTitles.length+1; // titles + header view.
    }


    // Check what type of view is being passed
    @Override
    public int getItemViewType(int position) {
        if (isPositionHeader(position))
            return TYPE_HEADER;

        return TYPE_ITEM;
    }

    @Override
    public void onViewRecycled(ViewHolder holder) {
        Log.d(TAG,"onViewRecycled");
        super.onViewRecycled(holder);
    }

    private boolean isPositionHeader(int position) {
        return position == 0;
    }

}

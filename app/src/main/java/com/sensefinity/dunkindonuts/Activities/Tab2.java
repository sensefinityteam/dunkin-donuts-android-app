package com.sensefinity.dunkindonuts.Activities;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.sensefinity.dunkindonuts.R;

/**
 * Created by RuiPires on 19/11/2015.
 */
public class Tab2 extends Fragment {

    public static ListView tab2ListView;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v =inflater.inflate(R.layout.tab1,container,false);

        // "Get" fragment specific listView
        tab2ListView = (ListView) v.findViewById(R.id.tab1_listView);

        // Associate listView-Adapter
        tab2ListView.setAdapter(NewOrderActivity.tab2ItemListAdapter);

        return v;
    }
}

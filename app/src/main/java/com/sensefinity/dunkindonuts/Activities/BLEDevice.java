package com.sensefinity.dunkindonuts.Activities;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.le.ScanRecord;

import java.util.Arrays;

public class BLEDevice {

    private BluetoothDevice bluetoothDevice;
    private ScanRecord scanRecord;
    private int rssi, type;
    private long timestamp;

    public BLEDevice() {

    }

    public BLEDevice(BluetoothDevice bluetooth_device, ScanRecord scan_record, int rssi, long timestamp) {

        this.bluetoothDevice = bluetooth_device;
        this.scanRecord = scan_record;
        this.rssi = rssi;
        this.timestamp = timestamp;
    }

    public BluetoothDevice getBLEDevice() {

        return bluetoothDevice;
    }

    public void setBLEDevice(BluetoothDevice bluetooth_device) {

        this.bluetoothDevice = bluetooth_device;
    }

    public ScanRecord getScanRecord() {

        return this.scanRecord;
    }

    public void setScanRecord(ScanRecord scan_record) {

        this.scanRecord = scan_record;
    }

    //TODO Acabar
    public String parseAdvertisementPacket(byte[] scan_record) {

        int i = 0;
        String a = new String("");

        a = Arrays.toString(scan_record);

        for (byte b : scan_record) {

            type = b;
        }

        return a;
    }

    public int getRSSI() {

        return rssi;
    }

    public void setRSSI(int rssi) {

        this.rssi = rssi;
    }

    public long getTimestamp() {

        return timestamp;
    }

    public void setTimestamp(long timestamp) {

        this.timestamp = timestamp;
    }
}
package com.sensefinity.dunkindonuts.Activities;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ListView;

import com.sensefinity.dunkindonuts.R;

/**
 * Created by RuiPires on 19/11/2015.
 */
public class Tab1 extends Fragment {

    public static ListView tab1ListView;
    Context ctx;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v =inflater.inflate(R.layout.tab1,container,false);


        ctx = getActivity().getApplicationContext();
        /*Toast.makeText(ctx, "TOAST", Toast.LENGTH_SHORT).show();
        */

        // "Get" fragment specific listView
        tab1ListView = (ListView) v.findViewById(R.id.tab1_listView);

        // Associate listView-Adapter
        tab1ListView.setAdapter(NewOrderActivity.tab1ItemListAdapter);


        return v;
    }

    /*
    @Override
    public void onItemClick(AdapterView<ProductItem> parent, View view, int position, long id) {
        long viewId = view.getId();

        if (viewId == R.id.product_add_button) {
            Toast.makeText(ctx, "Button 1 clicked", Toast.LENGTH_SHORT).show();
        }else {
            Toast.makeText(ctx, "ListView clicked", Toast.LENGTH_SHORT).show();
        }
    }
    */
}

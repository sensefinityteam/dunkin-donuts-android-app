package com.sensefinity.dunkindonuts.Activities;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.sensefinity.dunkindonuts.OnTaskCompleted;
import com.sensefinity.dunkindonuts.OrderProductItemListAdapter;
import com.sensefinity.dunkindonuts.ProductItem;
import com.sensefinity.dunkindonuts.R;
import com.sensefinity.dunkindonuts.productsTabs.GetProductsFromMachinates;
import com.sensefinity.dunkindonuts.productsTabs.SlidingTabLayout;
import com.sensefinity.dunkindonuts.productsTabs.ViewPagerAdapter;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;


public class NewOrderActivity extends BaseActivity implements OnTaskCompleted {

    private static final String TAG = NewOrderActivity.class.getName();
    public static boolean GetProductsFromMachinatesDone = false;

    // Declaring Your View and Variables
    Toolbar toolbar;
    ViewPager pager;
    ViewPagerAdapter adapter;
    SlidingTabLayout tabs;
    CharSequence Titles[] = {"Yogurts","Beverages"};
    int Numboftabs = 2;
    boolean addedYonestProducts = false;

    static OrderProductItemListAdapter tab1ItemListAdapter;

    static OrderProductItemListAdapter tab2ItemListAdapter;

    static OrderProductItemListAdapter tab3ItemListAdapter;


    public static ArrayList<ProductItem> beveragesList = new ArrayList<ProductItem>();

    public static ArrayList<ProductItem> bakeryList = new ArrayList<ProductItem>();

    public static ArrayList<ProductItem> sandwichesList = new ArrayList<ProductItem>();

    static ArrayList<ProductItem> mybag  = new ArrayList<ProductItem>();

    static TextView nProducts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        beveragesList.clear();
        bakeryList.clear();

        Log.d(TAG, "New order onCreate");
        super.onCreate(savedInstanceState);

        /* Adding  layout to "parent" class frame layout. */
        getLayoutInflater().inflate(R.layout.activity_new_order, frameLayout);

        // Creating The Toolbar and setting it as the Toolbar for this activity
        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);

        //Set toolbar logo
        toolbar.setLogo(R.drawable.ic_launcher);
        toolbar.setNavigationIcon(R.drawable.ic_menu_white_18dp);

        nProducts = (TextView) findViewById(R.id.num_produts_toolbar_tview);

        bagStats();

        //TODO logo -> navigator drawer
        // Enable ActionBar app icon to behave as action to toggle nav drawer
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        // Clear toolbar title
        getSupportActionBar().setTitle(Html.fromHtml("<font color='@android:color/white'>&nbsp;&nbsp;&nbsp;Products</font>"));

        //getSupportActionBar().setDisplayShowTitleEnabled(false);

        // Creating the ViewPagerAdapter and Passing Fragment Manager, Titles and number of Tabs
        adapter =  new ViewPagerAdapter(getSupportFragmentManager(),Titles,Numboftabs);

        // Assigning ViewPager View and setting the adapter
        pager = (ViewPager) findViewById(R.id.pageview);
        pager.setAdapter(adapter);

        // Assiging the Sliding Tab Layout View
        tabs = (SlidingTabLayout) findViewById(R.id.tab);

        // To make the Tabs Fixed and equal
        tabs.setDistributeEvenly(true);

        // Setting Custom Color for the Scroll bar indicator of the Tab View
        tabs.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
            @Override
            public int getIndicatorColor(int position) {
                return getResources().getColor(R.color.colorSensePurple);
            }
        });

        // Setting the ViewPager For the SlidingTabsLayout
        tabs.setViewPager(pager);

        // Associate product list to adapter
        tab1ItemListAdapter = new OrderProductItemListAdapter(this,1, (List) bakeryList);

        // Associate product list to adapter
        tab2ItemListAdapter = new OrderProductItemListAdapter(this,1, (List) beveragesList);

        // TODO GET  specific from WEB

        Log.d(TAG, "GetProductsFromMachinatesDone=" + GetProductsFromMachinatesDone);
        // GET FROM MACHINATES
        if(!GetProductsFromMachinatesDone) {

            Log.d(TAG, "Trying to load Products list, please wait");
            Toast.makeText(this,"Loading products. Please wait.",Toast.LENGTH_LONG).show();

            if(!addedYonestProducts) {

                GetProductsFromMachinates task = new GetProductsFromMachinates(this);
                task.execute(new String[]{});
                addedYonestProducts = true;
            }

            //addYonestProducts();
        }else{

            //adapter.notifyDataSetChanged();

            //((BaseAdapter)Tab1.tab1ListView.getAdapter()).notifyDataSetChanged();

        }


        if(beveragesList.size()==0)  addDunkinProducts();

    }

    public void UpdateTabs() {

        //tab1ItemListAdapter.Load();

        ((BaseAdapter)Tab1.tab1ListView.getAdapter()).notifyDataSetChanged();
        ((BaseAdapter)Tab2.tab2ListView.getAdapter()).notifyDataSetChanged();
    }

    private static void bagStats() {
        // Update
        nProducts.setText("" + mybag.size());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){

        Log.d(TAG,"onOptionsItemSelected()");
        Intent intent;
        switch(item.getItemId()){
            case R.id.menu_bag:  // Go to myBagActivity
                intent = new Intent(this, MyBagActivity.class);
                intent.setClass(NewOrderActivity.this,MyBagActivity.class);
                // Create a Bundle and Put Bundle in to it
                Bundle bundleObject = new Bundle();

                bundleObject.putSerializable("list", mybag);
                intent.putExtras(bundleObject);

                this.startActivity(intent);
                return true;

            default: return super.onOptionsItemSelected(item);
        }
    }

    public static void addProduct(ProductItem product){
        mybag.add(product);
        bagStats();
    }

    @Override
    protected void onRestart(){
        Log.d(TAG, "New order onRestart");
        super.onRestart();

    }

    @Override
    protected void onResume() {
        Log.d(TAG, "New order onResume");
        bagStats();
        super.onResume();

    }



    @Override //implements OnTaskCompleted
    public void onTaskCompleted(Boolean taskStatus) {

        //TODO TOAST
        if(taskStatus){

            Log.d(TAG, "Task return True notifyDataSetChanged");
            //adapter.notifyDataSetChanged();

        }else{

            //Toast.makeText(this, "Cannot load products. Do you have internet connection?", Toast.LENGTH_SHORT).show();
        }

    }

    public void addYonestProducts(){

            ProductItem new_product = new ProductItem("Natural", 2.50);
            Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.natural);
            new_product.setBitmapDrawable(bitmap);
            bakeryList.add(new_product);

            ProductItem new_product6 = new ProductItem("Roasted apple", 4.45);
            Bitmap bitmap6 = BitmapFactory.decodeResource(getResources(), R.drawable.roastedapple);
            new_product6.setBitmapDrawable(bitmap6);
            bakeryList.add(new_product6);


        ProductItem new_product3 = new ProductItem("Lemon", 3.75);
        Bitmap bitmap3 = BitmapFactory.decodeResource(getResources(), R.drawable.lemon);
        new_product3.setBitmapDrawable(bitmap3);
        bakeryList.add(new_product3);

        ProductItem new_product4 = new ProductItem("Mango", 3.99);
        Bitmap bitmap4 = BitmapFactory.decodeResource(getResources(), R.drawable.mango);
        new_product4.setBitmapDrawable(bitmap4);
        bakeryList.add(new_product4);

        ProductItem new_product5 = new ProductItem("Dry fruits", 4.25);
        Bitmap bitmap5 = BitmapFactory.decodeResource(getResources(), R.drawable.dryfruits);
        new_product5.setBitmapDrawable(bitmap5);
        bakeryList.add(new_product5);

        ProductItem new_product2 = new ProductItem("Shake", 3.50);
        Bitmap bitmap2 = BitmapFactory.decodeResource(getResources(), R.drawable.shake);
        new_product2.setBitmapDrawable(bitmap2);
        beveragesList.add(new_product2);
    }

    public void addDunkinProducts(){

        /*
        ProductItem  newProduct = new ProductItem("Cappuccino",1.0);
        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.dd_logo);

        newProduct.setBitmapDrawable(new BitmapDrawable(getResources(),bitmap));
        beveragesList.add(newProduct);

        newProduct= new ProductItem("Dunkaccino®",1.2);
        beveragesList.add(newProduct);

        newProduct= new ProductItem("Espresso",1.0);
        bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.dd_logo);

        newProduct.setBitmapDrawable(new BitmapDrawable(getResources(),bitmap));
        beveragesList.add(newProduct);

        newProduct= new ProductItem("Hot Chocolate",1.0);
        beveragesList.add(newProduct);

        newProduct= new ProductItem("Hot Tea",1.5);
        beveragesList.add(newProduct);

        newProduct= new ProductItem("Vanilla Chai",1.0);
        beveragesList.add(newProduct);

        //////////  -> BAKERY

        newProduct= new ProductItem("Donuts",1.2);
        bakeryList.add(newProduct);

        newProduct= new ProductItem("Munchkins®",1.0);
        bakeryList.add(newProduct);

        newProduct= new ProductItem("Muffins",1.0);
        bakeryList.add(newProduct);

        newProduct= new ProductItem("Danishes",1.5);
        bakeryList.add(newProduct);

        //////////  -> Sandwiches

        newProduct= new ProductItem("Bacon Egg and Cheese",1.2);
        sandwichesList.add(newProduct);

        newProduct= new ProductItem("Big N' Toasted®",1.0);
        sandwichesList.add(newProduct);

        newProduct= new ProductItem("Egg White Flatbread",1.0);
        sandwichesList.add(newProduct);

        newProduct= new ProductItem("Ham Egg and Cheese",1.5);
        sandwichesList.add(newProduct);

        */
    }
}

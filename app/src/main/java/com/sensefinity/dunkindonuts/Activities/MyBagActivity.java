package com.sensefinity.dunkindonuts.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.sensefinity.dunkindonuts.ProductItem;
import com.sensefinity.dunkindonuts.R;
import com.sensefinity.dunkindonuts.myBagProductsListAdapter;

import java.util.ArrayList;

/**
 * Created by RuiPires on 25/11/2015.
 */
public class MyBagActivity extends BaseActivity {

    private static final String TAG = MyBagActivity.class.getName();

    // Declaring Your View and Variables
    Toolbar toolbar;
    static public ArrayList<ProductItem> myBagProducts;
    static public ListView bagListView;

    myBagProductsListAdapter myBagAdapter;

    private TextView nproducts;
    private static TextView totalPrice;

    FloatingActionButton payButton;
    private MyBagActivity contex;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "My bag onCreate");
        super.onCreate(savedInstanceState);

        contex=this;
        /* Adding  layout to "parent" class frame layout. */
        getLayoutInflater().inflate(R.layout.activity_mybag, frameLayout);

        // Creating The Toolbar and setting it as the Toolbar for this activity
        toolbar = (Toolbar) findViewById(R.id.mybag_toolbar);
        setSupportActionBar(toolbar);

        //toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);
        //toolbar.setNavigationIcon(R.drawable.ic_coffe);

        //Set toolbar logo
        toolbar.setLogo(R.drawable.ic_launcher);
        toolbar.setNavigationIcon(R.drawable.ic_menu_white_18dp);

        //TODO logo -> navigator drawer
        // Enable ActionBar app icon to behave as action to toggle nav drawer
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        // Clear toolbar title
        //getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setTitle(Html.fromHtml("<font color='@android:color/white'>&nbsp;&nbsp;&nbsp;My Bag</font>"));

        myBagProducts = new ArrayList<ProductItem>();

        try{
            // Get the Bundle Object
            Bundle bundleObject = getIntent().getExtras();

            // Get ArrayList Bundle
            ArrayList<ProductItem> classObject = (ArrayList<ProductItem>) bundleObject.getSerializable("list");

            //Retrieve Objects from Bundle
            for(int index = 0; index < classObject.size(); index++){

                ProductItem object = classObject.get(index);
                myBagProducts.add(object);
            }
        } catch(Exception e){
            e.printStackTrace();
        }

        bagListView = (ListView) findViewById(R.id.myBadListView);

        myBagAdapter = new myBagProductsListAdapter(this,1,myBagProducts);
        // Associate listView-Adapter
        bagListView.setAdapter(myBagAdapter);

        totalPrice = (TextView) findViewById(R.id.totalTextViewEdit);

        updateTotal();
       // Log.d(TAG, "intent extra"+ data.get(0).getName() + data.get(0).getType());

        payButton= (FloatingActionButton) findViewById(R.id.checkoutButton);

        payButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Log.d(TAG, "Pay button clicked");

                Intent intent = new Intent();
                intent.setClass(MyBagActivity.this,OrderStatusActivity.class);

                // Create a Bundle and Put Bundle in to it
                Bundle bundleObject = new Bundle();
                bundleObject.putSerializable("list", myBagProducts);
                intent.putExtra("key", bundleObject);
                contex.startActivity(intent);

            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
      //  getMenuInflater().inflate(R.menu.menu, menu);
      //       return super.onCreateOptionsMenu(menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        return super.onOptionsItemSelected(item);
    }



    private static void updateTotal() {

        // Run list mybag and set mybag stats text views
        Double total = 0.0;
        //Retrieve Objects from Bundle
        for(int index = 0; index < myBagProducts.size(); index++){
            total += myBagProducts.get(index).getPrice();
        }

       totalPrice.setText(new String().format("%.2f", total) + "€");
    }


    public static void deleteProduct(int index){

        NewOrderActivity.mybag.remove(index);
        myBagProducts.remove(index);

        ((BaseAdapter)bagListView.getAdapter()).notifyDataSetChanged();

        updateTotal();
    }

}

package com.sensefinity.dunkindonuts.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.sensefinity.dunkindonuts.R;

/**
 * Created by RuiPires on 13/11/2015.
 */
public class MainActivity extends BaseActivity {

    private static final String TAG = MainActivity.class.getName();
    private int imageArray[] = {R.drawable.banner9,R.drawable.banner10,R.drawable.banner12};
    private int i = 0;
    private ViewPager myPager;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "Main activity onCreate");
        super.onCreate(savedInstanceState);

        /**
         * Adding layout to parent class frame layout.
         */
        getLayoutInflater().inflate(R.layout.activity_main, frameLayout);

        /*      Toolbar     */
        // Creating The Toolbar and setting it as the Toolbar for this activity
        toolbar = (Toolbar) findViewById(R.id.include_toolbar_main);
        setSupportActionBar(toolbar);

        //Set toolbar logo and icon
        toolbar.setLogo(R.drawable.ic_launcher);
        toolbar.setNavigationIcon(R.drawable.ic_menu_white_18dp);

        toolbar.getNavigationIcon().setLevel(5);

        getSupportActionBar().setHomeButtonEnabled(true);

        // Clear toolbar title
        //getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setTitle(Html.fromHtml("<font color='@android:color/white'>&nbsp;&nbsp;&nbsp;</font>"));


        ViewPagerAdapter adapter = new ViewPagerAdapter(this, imageArray);
        myPager = (ViewPager) findViewById(R.id.myfivepanelpager);
        myPager.setAdapter(adapter);
        myPager.setCurrentItem(i);
        handler.postDelayed(runnable, 4000);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.newOrderButton);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent new_intent = new Intent(MainActivity.this, NewOrderActivity.class);
                MainActivity.this.startActivity(new_intent);
            }
        });
    }

    final Handler handler = new Handler();
    Runnable runnable = new Runnable() {
        public void run() {
            myPager.setCurrentItem(i);
            i++;
            if(i==3)
            {
                i=0;
            }
            handler.postDelayed(this, 4000);
        }
    };


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();

        if(Drawer.isDrawerOpen(mRecyclerView)){
            Drawer.closeDrawer(mRecyclerView);
        }
    }

    boolean doubleBackToExitPressedOnce = false;

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Click back again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }
}
package com.sensefinity.dunkindonuts.Activities;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Messenger;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.sensefinity.dunkindonuts.OrderItemListAdapter;
import com.sensefinity.dunkindonuts.ProductItem;
import com.sensefinity.dunkindonuts.R;
import com.sensefinity.dunkindonuts.myBagProductsListAdapter;

import java.util.ArrayList;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class OrderStatusActivity extends BaseActivity {

    private ArrayList<ProductItem> orderProducts;

    private int BLE_REQUEST_ENABLE = 1;
    private static final String TAG = OrderStatusActivity.class.getName();
    private Toolbar toolbar;

    private ListView orderListView;
    private OrderItemListAdapter orderListAdapter;
    private Boolean orderPrepared = false;

    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        context= this;
        /* Adding  layout to "parent" class frame layout. */
        getLayoutInflater().inflate(R.layout.activity_order_status, frameLayout);

        // Creating The Toolbar and setting it as the Toolbar for this activity
        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);

        //Set toolbar logo
        toolbar.setLogo(R.drawable.ic_launcher);
        toolbar.setNavigationIcon(R.drawable.ic_menu_white_18dp);

        getSupportActionBar().setHomeButtonEnabled(true);

        // Clear toolbar title
        getSupportActionBar().setTitle(Html.fromHtml("<font color='@android:color/white'>&nbsp;&nbsp;&nbsp;Your Order </font>"));

        orderProducts= new ArrayList<ProductItem>();
        try{
            // Get the Bundle Object
            Bundle bundleObject = getIntent().getBundleExtra("key");

            // Get ArrayList Bundle
            ArrayList<ProductItem> classObject = (ArrayList<ProductItem>) bundleObject.getSerializable("list");

            //Retrieve Objects from Bundle
            for(int index = 0; index < classObject.size(); index++){

                ProductItem object = classObject.get(index);
                orderProducts.add(object);
            }
        } catch(Exception e){
            Log.d(TAG, "GET products on create");
            e.printStackTrace();
        }

        orderListView = (ListView) findViewById(R.id.orderListView);

        orderListAdapter = new OrderItemListAdapter(this,1,orderProducts);
        // Associate listView-Adapter
        orderListView.setAdapter(orderListAdapter);

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

                orderPreparedUpdate();
            }
        }, 5000);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    public void orderPreparedUpdate(){

        TextView statusTV = (TextView) findViewById(R.id.order_status_textView);
        statusTV.setText("Ready!");

        ImageView statusIV = (ImageView) findViewById(R.id.orderStatusImageView);
        statusIV.setImageDrawable(getResources().getDrawable(R.drawable.ic_order_complete));

        RelativeLayout rL = (RelativeLayout) findViewById(R.id.statusRelativeLayout);

        //rL.setBackgroundColor(getResources().getColor(R.color.greenOrderComplete));

        final Handler handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {

                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        StopService();

                        orderDeliveringUpdate();
                    }
                }, 2000);
            }
        };

        TurnBLEOn();
        Intent intent = new Intent(this, BLEService.class);

        Messenger messenger = new Messenger(handler);
        intent.putExtra("messenger", messenger);

        startService(intent);

        View.OnClickListener onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                StopService();
                orderDeliveringUpdate();
            }
        };

        rL.setOnClickListener(onClickListener);
    }

    public void StopService() {

        stopService(new Intent(this, BLEService.class));
    }

    public void orderDeliveringUpdate(){

        TextView statusTV = (TextView) findViewById(R.id.order_status_textView);
        statusTV.setText("Delivering...");

        ImageView statusIV = (ImageView) findViewById(R.id.orderStatusImageView);
        statusIV.setImageDrawable(getResources().getDrawable(R.drawable.plane));

        RelativeLayout rL = (RelativeLayout) findViewById(R.id.statusRelativeLayout);
        rL.setBackgroundColor(getResources().getColor(R.color.colorSensePurple));

        //orderPrepared = true;

        //View.OnClickListener onClickListener = new View.OnClickListener() {
            //@Override
            //public void onClick(View view) {
                //Log.d(TAG, "Onclick Listener Relative Layout");

                //Intent intent= new Intent();
                //intent.setClass(context,MainActivity.class);
                //intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                //startActivity(intent);

                //NewOrderActivity.mybag.clear();
            //}
        //};

        //rL.setOnClickListener(onClickListener);

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

                orderFinishedUpdate();
            }
        }, 5000);
    }

    public void orderFinishedUpdate(){

        TextView statusTV = (TextView) findViewById(R.id.order_status_textView);
        statusTV.setText("Finished!");

        ImageView statusIV = (ImageView) findViewById(R.id.orderStatusImageView);
        statusIV.setImageDrawable(getResources().getDrawable(R.drawable.deal));

        RelativeLayout rL = (RelativeLayout) findViewById(R.id.statusRelativeLayout);
        //rL.setBackgroundColor(getResources().getColor(R.color.greenOrderComplete));

        orderPrepared = true;

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

                Restart();
            }
        }, 2000);
    }

    public void Restart(){

        Intent intent= new Intent();
        intent.setClass(context, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);

        NewOrderActivity.mybag.clear();
    }

    @Override
    public void onBackPressed() {

        if(orderPrepared){
            return;
        }else{
        super.onBackPressed();
        }
    }



    public void TurnBLEOn() {

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN_MR2) {

            final BluetoothManager bluetooth_manager =
                    (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
            BluetoothAdapter bleAdapter = null;
                bleAdapter = bluetooth_manager.getAdapter();

            if ((!getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) &&
                    (bleAdapter == null)) {

                return;
            }

            if (!bleAdapter.isEnabled()) {

                Intent turn_on_intent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(turn_on_intent, BLE_REQUEST_ENABLE);
            }
        }
    }
}
package com.sensefinity.dunkindonuts.Activities;

import android.os.Bundle;
import android.util.Log;

import com.sensefinity.dunkindonuts.R;

/**
 * Created by RuiPires on 13/11/2015.
 */
public class ProfileActivity extends BaseActivity {

    private static final String TAG = ProfileActivity.class.getName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "Profile activity onCreate");
        super.onCreate(savedInstanceState);

        /* Adding our layout to parent class frame layout.*/
        getLayoutInflater().inflate(R.layout.activity_profile, frameLayout);
    }

}

package com.sensefinity.dunkindonuts.Activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.sensefinity.dunkindonuts.R;
import com.sensefinity.dunkindonuts.productsTabs.GetProductsFromMachinates;

public class SplashScreenActivity extends Activity {

    @Override
    protected void onCreate(Bundle saved_instance_state) {

        super.onCreate(saved_instance_state);
        setContentView(R.layout.activity_splash_screen);

        Thread timerThread = new Thread(){

            public void run(){

                try{

                    // TODO
                    /*
                    GetProductsFromMachinates task = new GetProductsFromMachinates();
                    task.execute(new String[]{});
                    */

                    sleep(3000);
                }catch(InterruptedException e){

                    e.printStackTrace();
                }finally{

                    Intent intent = new Intent(SplashScreenActivity.this, MainActivity.class);
                    startActivity(intent);
                }
            }
        };

        timerThread.start();
    }

    @Override
    protected void onPause() {

        super.onPause();

        finish();
    }
}
package com.sensefinity.dunkindonuts.Activities;

import android.content.Intent;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.FrameLayout;

import com.sensefinity.dunkindonuts.R;

/**
 * Created by RuiPires on 13/11/2015.
 */

/*
* Base Activity, this activity will implement the basic functionality of the navigator drawer,
* all others activities will extend from them.
* */
public class BaseActivity extends ActionBarActivity {

    private static boolean isLaunch = false;
    public FrameLayout frameLayout;

    /* Declaration of Titles And Icons for the Navigation Drawer List View*/
    String TITLES[] = {"Home","Buy","My Bag","My Profile","Settings"};
    int ICONS[] = {R.drawable.ic_home,R.drawable.ic_coffe,R.drawable.ic_carrinho,
            R.drawable.ic_action_user, R.drawable.ic_settings};

    /*Load user Profile*/
    //Name and picture for the header view
    String NAME = "Meireles";
    int PROFILE = R.drawable.uiface128;

    private Toolbar toolbar;                              // Declaring the Toolbar Object

    static RecyclerView mRecyclerView;                    // Declaring RecyclerView
    RecyclerView.Adapter mAdapter;                        // Declaring Adapter for the recycler view
    RecyclerView.LayoutManager mLayoutManager;            // Declaring Layout Manager as a linear layout manager

    /** A LayoutManager is responsible for measuring and positioning item views within
     * a RecyclerView as well as determining the policy for when to recycle item views
     * that are no longer visible to the user. */

    public static DrawerLayout   Drawer;                  // Declaring DrawerLayout

    ActionBarDrawerToggle mDrawerToggle;                  // Declaring Action Bar Drawer Toggle


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);

        frameLayout = (FrameLayout)findViewById(R.id.content_frame);

        mRecyclerView = (RecyclerView) findViewById(R.id.RecyclerView);

        mRecyclerView.setHasFixedSize(true);            // Changes in adapter content cannot change the size of the RecyclerView itself.

        mAdapter = new MyAdapter(TITLES,ICONS,NAME,PROFILE,this);       // Creating the Adapter MyAdapter

        mRecyclerView.setAdapter(mAdapter);                              // Setting the adapter to RecyclerView

        mLayoutManager = new LinearLayoutManager(this);                 // Creating a layout Manager
        mRecyclerView.setLayoutManager(mLayoutManager);                 // Setting the layout Manager


        Drawer = (DrawerLayout) findViewById(R.id.DrawerLayout);        // Drawer object assigned to the view

/*
        // Creating The Toolbar and setting it as the Toolbar for the activity

        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);

        //toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);
        toolbar.setNavigationIcon(R.drawable.ic_coffe);
        toolbar.setLogo(R.drawable.ic_coffe);

        // enable ActionBar app icon to behave as action to toggle nav drawer
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);


        //TODO definir como usar a action bar
        mDrawerToggle = new ActionBarDrawerToggle(this,Drawer,toolbar,R.string.openDrawer,R.string.closeDrawer){

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                // code here will execute once the drawer is opened
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                // Code here will execute once drawer is closed
            }

        };

       Drawer.setDrawerListener(mDrawerToggle);    // Drawer Listener set to the Drawer toggle
       mDrawerToggle.syncState();                   // Finally we set the drawer toggle sync State

        */
        if(!isLaunch){
            /* When aplication is launch will be "redirect" to the main activity */
            isLaunch = true;
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        //int id = item.getItemId();

        Log.d("BASE ACTIVITY", "item.getOrder()=" + item.getOrder());
        Log.d("BASE ACTIVITY","item.getItemId()" + item.getItemId());
        Log.d("BASE ACTIVITY", "onOptionsItemSelected");

        //Click on navigator icon -> OpenDrawer
        if(item.getOrder() == 0){
            Log.d("BASE ACTIVITY", "Drawer.openDrawer()");
            Drawer.openDrawer(mRecyclerView);
        }

        return true;
    }

    public static void closeNavigatorDrawer(){
        if(Drawer.isDrawerOpen(mRecyclerView)){
            Drawer.closeDrawer(mRecyclerView);
        }
        return;
    }

    @Override
    protected void onResume() {
        super.onResume();
    }
}

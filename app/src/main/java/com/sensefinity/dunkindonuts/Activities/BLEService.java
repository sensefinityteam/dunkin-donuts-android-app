package com.sensefinity.dunkindonuts.Activities;

import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanRecord;
import android.bluetooth.le.ScanResult;
import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;

import java.util.ArrayList;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothAdapter.LeScanCallback;
import android.os.Handler;
import android.annotation.TargetApi;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;
import android.widget.Toast;

import com.sensefinity.dunkindonuts.Activities.BLEDevice;

@TargetApi(21)
public class BLEService extends Service {

    private static final String TAG = BLEService.class.getName();

    private Handler handler;
    private int scanInterval = 0;

    private BluetoothAdapter bleAdapter;
    private int BLE_REQUEST_ENABLE = 1;
    private boolean bleOn = false;
    private boolean bleScanning = false;
    private static final long BLE_SCAN_PERIOD = 15000;
    private ArrayList<BLEDevice> bleDevices;

    private Messenger messenger;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        if (intent != null)
            messenger = (Messenger) intent.getParcelableExtra("messenger");

        final BluetoothManager bluetooth_manager =
                (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        bleAdapter = bluetooth_manager.getAdapter();

        bleDevices = new ArrayList<BLEDevice>();

        handler = new Handler();
        startScanCheckerTask();

        return START_NOT_STICKY;
    }

    @Override
    public void onCreate() {

    }

    @Override
    public IBinder onBind(Intent intent) {

        throw new UnsupportedOperationException("Not yet implemented");
    }

    private void runOnUiThread(Runnable runnable) {

        handler.post(runnable);
    }

    Runnable scanChecker = new Runnable() {

        @Override
        public void run() {

            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN_MR2)
                leScanBLEDevices(true);

            handler.postDelayed(scanChecker, scanInterval);
        }
    };

    void startScanCheckerTask() {

        scanChecker.run();
    }

    void stopScanCheckerTask() {

        handler.removeCallbacks(scanChecker);
    }

    public void bleOn() {

        bleOn = true;
    }

    public void bluetoothOff() {

        bleAdapter.disable();

        bleOn = false;
        bleDevices.clear();
    }

    public ArrayList<BLEDevice> getBLEDevices() {

        return bleDevices;
    }

    public void AddBLEDevice(BluetoothDevice bluetooth_device,
                             ScanRecord scan_record,
                             int rssi,
                             long timestamp) {

        for (BLEDevice ble_device : bleDevices)
            if(ble_device.getBLEDevice().getAddress().equals(bluetooth_device.getAddress()))
                return;

        BLEDevice new_ble_device = new BLEDevice(bluetooth_device,
                scan_record,
                rssi,
                timestamp);
        bleDevices.add(new_ble_device);
    }

    /*private void scanBLEDevices(final boolean enable) {

        if(enable) {

            handler.postDelayed(new Runnable() {

                @Override
                public void run() {

                    bleScanning = false;
                    bleAdapter.getBluetoothLeScanner().stopScan(mScanCallback);
                }
            }, BLE_SCAN_PERIOD);

            bleDevices.clear();
            bleScanning = true;

            bleAdapter.getBluetoothLeScanner().startScan(mScanCallback);
        } else {

            bleScanning = false;
            bleAdapter.getBluetoothLeScanner().stopScan(mScanCallback);
        }
    }

    private ScanCallback mScanCallback =
            new ScanCallback() {

                @Override
                public void onScanResult(int call_back_type, final ScanResult result) {

                    runOnUiThread(new Runnable() {

                        @Override
                        public void run() {

                            AddBLEDevice(result.getDevice(),
                                    result.getScanRecord(),
                                    result.getRssi(),
                                    result.getTimestampNanos());
                        }
                    });
                }
            };*/

    private void leScanBLEDevices(final boolean enable) {

        if (enable) {

            handler.postDelayed(new Runnable() {

                @Override
                public void run() {

                    bleScanning = false;
                    bleAdapter.stopLeScan(mLeScanCallback);
                }
            }, BLE_SCAN_PERIOD);

            bleDevices.clear();
            bleScanning = true;

            bleAdapter.startLeScan(mLeScanCallback);
        } else {

            bleScanning = false;
            bleAdapter.stopLeScan(mLeScanCallback);
        }
    }

    private LeScanCallback mLeScanCallback =
            new LeScanCallback() {

                @Override
                public void onLeScan(final BluetoothDevice device, final int rssi, final byte[] scanRecord) {

                    runOnUiThread(new Runnable() {

                        @Override
                        public void run() {

                            AddBLEDevice(device,
                                    null,
                                    rssi,
                                    0);

                            if(device.getAddress().equals("A4:ED:61:86:DA:FA")) {
                            //if(bleDevices.size() == 1) {

                                final Message message = Message.obtain(null, 1234);

                                try {

                                    messenger.send(message);
                                } catch (RemoteException exception) {

                                    exception.printStackTrace();
                                }
                            }
                        }
                    });
                }
            };
}